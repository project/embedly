-- SUMMARY --

The Embedly module provides integration with embed.ly.

For a full description of the module, visit the project page:
  http://drupal.org/project/embedly

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/embedly

-- INSTALLATION --

* Install as usual, see https://www.drupal.org/node/1897420.

-- CONFIGURATION --

* Configure the Embedly settings at
  Administration » Configuration » Web services:

  - API key

    The API key is provided by Embedly. You can grab your key from the Embedly
    dashboard at https://app.embed.ly.

-- TROUBLESHOOTING --

* If the Embedly module does not work, check the following:

  - Make sure you have entered a valid API key.

-- THEMING --

* Create a template named embedly.html.twig in your theme to customize
  the display of the embedded content.

* Clear your cache.

-- CONTACT --

Current maintainers:
* Irfaan Chummun - http://drupal.org/user/3533879
